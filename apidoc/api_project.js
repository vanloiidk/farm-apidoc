define({
  "name": "Cooperative management platform",
  "version": "1.0.0",
  "description": "CMP APIs documentation",
  "title": "Cooperative management platform | APIs documentation",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-03-24T14:06:56.911Z",
    "url": "http://apidocjs.com",
    "version": "0.19.1"
  }
});
